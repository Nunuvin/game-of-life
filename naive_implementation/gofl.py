''' game-of-life
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
import random
class Board:
    '''the game board. Grid representation of the board with each
       cell being an object
       access change_state to change state of a cell
       check_rules every turn
    '''
    def generate_board(self, randomize = False):
        '''
            generates board given self.height, self.width
        '''
        self.board=[]
        for h in range(self.height):
            row=[]
            for w in range(self.width):
                if randomize == True:
                    rnd = random.uniform(0,1)
                    if rnd >= 0.5:
                        row.append(1)
                    else:
                        row.append(0)
                else:
                    row.append(0)
            self.board.append(row)

    def __init__(self, width, height, randomize = False, restart = False):
        '''
            initializes an instance of a board and generates board given
            widht and height
        '''
        self.restart = restart
        self.width = width
        self.height = height
        self.generate_board(randomize)

    def change_state(self, coordinates):
        '''
            takes in coordinates [x,y] of a cell to change state
        '''
        if self.board[coordinates[0]][coordinates[1]] == 0:
            self.board[coordinates[0]][coordinates[1]] = 1
        elif self.board[coordinates[0]][coordinates[1]] == 1:
            self.board[coordinates[0]][coordinates[1]] = 0
    
    def count_alive_neighbours(self, cellCoord):
        '''
            generates list of all cell neighbours
        '''

        aliveNeighbours = 0
        checkIndecies = [-1, 0, 1]
        for y in checkIndecies:
            for x in checkIndecies:
                if 0 <= cellCoord[0] + y < self.height:
                    if 0 <= cellCoord[1] + x < self.width:
                        if self.board[cellCoord[0]+y][cellCoord[1]+x] == 1 and (x!=0 or y!=0):
                            aliveNeighbours += 1
        return aliveNeighbours

    def check_rules(self):
        ''' if cell is alive check if 2,3 neighbours are alive else die
            elif dead and surrounded by only 3 neighbours then becomes alive
        '''
        nboard = [[0 for i in range(self.width)] for j in range(self.height)]
        for h in range(self.height):
            for w in range(self.width):
                aliveNeighbours=self.count_alive_neighbours([h,w])
                if self.board[h][w] == 0 and aliveNeighbours == 3:
                    nboard[h][w] = 1
                elif  self.board[h][w] == 1 and (aliveNeighbours != 3 and aliveNeighbours != 2):
                    nboard[h][w] = 0
                else:
                    nboard[h][w] = self.board[h][w]
        if self.restart and self.board == nboard:
            empty = True
            for h in range(self.height):
                for w in range(self.width):
                    if self.board[h][w] != 0:
                        empty = False
            if not empty:   
                self.generate_board(True)
        else:
            self.board = nboard