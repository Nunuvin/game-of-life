''' game-of-life
    Copyright (C) 2017  Vlad C

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.'''
import pygame
import sys
from gofl import Board

pygame.init() #init pygame

pyDim = 720
pySurf = pygame.display.set_mode((pyDim, pyDim)) #setup window and caption on it
pygame.display.set_caption('Conways Game Of Life')

BLACK = (0,0,0) #setup tuples for colors
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

font = pygame.font.SysFont(None, 32)


def draw_board(surf, bd, cellDim, cellSpc):
    '''
        draws game board to the scrn
    '''
    #pySurf.fill(BLACK)
    for h in range(bd.height):
        for w in range(bd.width):
            if bd.board[h][w] == 1:
                topY = cellDim * h + cellSpc * h 
                topX = cellDim * w + cellSpc * w 
                pySurf.fill(WHITE, (topY, topX, cellDim, cellDim))
            else:
                topY = cellDim * h + cellSpc * h 
                topX = cellDim * w + cellSpc * w 
                pySurf.fill(BLACK, (topY, topX, cellDim, cellDim))
                pygame.display.update()

def console_dump(bd):
    '''
        draws bd in console
    '''
    for h in range(bd.height):
        for w in range(bd.width):
            print(bd.board[h][w], end='')
        print()

def main(surf, pyDim):
    '''
        orchestrates drawing and updating of the board
    '''
    print('''game-of-life  Copyright (C) 2017  Vlad C
             This program comes with ABSOLUTELY NO WARRANTY;
             This is free software, and you are welcome to redistribute it
             under certain conditions; for more details please see license or
             go to <http://www.gnu.org/licenses/>
             ''')
    print("Welcome to Conway's game of life")
    print("Controls:\nesc to pause, backspace to empty board, space to randomize board\n click to change cell state")
    boardDim = 36
    GB = Board(boardDim, boardDim, True, True)
    cellSpc = 1 #buffer around cells
    cellDim = pyDim//(boardDim + cellSpc)
    pause = False
    while True:        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                pos = [pos[0]//(cellSpc+cellDim),pos[1]//(cellSpc+cellDim)]
                if pos[0]<GB.height and pos[1]<GB.width:
                    GB.change_state(pos)
                draw_board(surf, GB, cellDim, cellSpc)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    GB.generate_board(True)
                elif event.key == pygame.K_ESCAPE:
                    pause = not pause
                    print('paused? ',pause)
                elif event.key == pygame.K_BACKSPACE:
                    GB.generate_board()
               
        if not pause:
            draw_board(surf, GB, cellDim, cellSpc)
            GB.check_rules()

main(pySurf, pyDim)